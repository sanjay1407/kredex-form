import React from 'react';
import { Field, reduxForm } from 'redux-form';
import validate from '../../validate';
import renderField from '../RenderField';
import PrimaryButton from '../PrimaryButton';

const DealCreationForm = props => {
  const { handleSubmit } = props;
  return (
    <form onSubmit={handleSubmit}>
      <Field
        name="dealName"
        type="text"
        component={renderField}
        label="Name"
        required
      />
      <Field
        name="listingDate"
        type="date"
        component={renderField}
        label="Date"
      />
       <Field
        name="dealAmount"
        type="number"
        min="0"
        step="any"
        component={renderField}
        label="Amount"
      />
      <div style={{marginTop:"20px"}}>
        <PrimaryButton type="submit" label="Next" />
      </div>
    </form>
  );
};

export default reduxForm({
  form: 'kredexForm', //             <------ same form name
  destroyOnUnmount: false, //        <------ preserve form data
  forceUnregisterOnUnmount: true, // <------ unregister fields on unmount
  validate,
})(DealCreationForm);
