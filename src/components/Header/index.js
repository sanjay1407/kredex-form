import React from 'react';
import styles from './styles';

const Header = () => {
    return (
        <div style={styles.header}>
            <h1>Kredex Deal Creation </h1>
        </div>
    );
}

export default Header;
