import {primaryColor, secondaryColor} from '../../theme';

const styles={
    header:{
        display:"flex",
        width: "100%",
        height:"50px",
        color:secondaryColor,
        backgroundColor:primaryColor,
        justifyContent:"center",
        alignItems:'center'
    }
}

export default styles;