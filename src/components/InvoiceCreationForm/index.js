import React from "react";
import { Field, reduxForm } from "redux-form";
import validate from "../../validate";
import renderField from "../RenderField";
import PrimaryButton from "../PrimaryButton";
import styles from './styles';

const InvoiceCreationForm = props => {
  const { handleSubmit, pristine, previousPage, submitting } = props;
  return (
    <form onSubmit={handleSubmit}>
      <div style={{display:"flex"}}>
        <div style={styles.leftContainer}>
          <Field
            name="invoiceName"
            type="text"
            component={renderField}
            label="Invoice Name"
          />
          <Field
            name="repaymentDate"
            type="date"
            component={renderField}
            label="Repayment Date"
          />
        </div>
        <div style={styles.rightContainer}>
          <Field
            name="issuedDate"
            type="date"
            component={renderField}
            label="Issued Date"
          />
          <Field
            name="invoiceAmount"
            type="number"
            min="0"
            step="any"
            component={renderField}
            label="Amount"
          />
        </div>
        </div>
        <div>
        <div style={styles.buttonContainer}>
          <div style={styles.leftButton}>
            <PrimaryButton
              type="button"
              onClick={previousPage}
              label="Previous"
            />
          </div>
          <div style={styles.rightButton}>
            <PrimaryButton
              type="submit"
              disabled={pristine || submitting}
              label="Submit"
            />
          </div>
        </div>
      </div>
    </form>
  );
};
export default reduxForm({
  form: "kredexForm", //                 <------ same form name
  destroyOnUnmount: false, //        <------ preserve form data
  forceUnregisterOnUnmount: true, // <------ unregister fields on unmount
  validate
})(InvoiceCreationForm);
