const styles = {
    leftContainer:{
        flex:1,
        marginRight: "20px",
        display: "inline-block",
        width: "45%"
    },
    rightContainer:{
        flex:1,
        display: "inline-block",
        width: "45%"
    },
    buttonContainer:{
        marginTop:"30px"
    },
    leftButton:{
        marginRight: "20px",
        display: "inline-block",
        width: "45%"
    },
    rightButton:{
        display: "inline-block",
        width: "45%"
    }
}

export default styles;