import React, { Component } from 'react';
import PropTypes from 'prop-types';
import DealCreationForm from '../DealCreationForm';
import InvoiceCreationForm from '../InvoiceCreationForm';
import StepsIndicator from '../StepsIndicator';
import styles from './styles';

class MainForm extends Component {
  constructor(props) {
    super(props);
    this.nextPage = this.nextPage.bind(this);
    this.previousPage = this.previousPage.bind(this);
    this.state = {
      page: 1,
    };
  }
  nextPage() {
    this.setState({ page: this.state.page + 1 });
  }

  previousPage() {
    this.setState({ page: this.state.page - 1 });
  }
  renderTitle=(page)=>{
      let title = page===1?'Deal Creation Page':'Invoice Creation Page';
        return(
            <div style={styles.formTitle}><h1>{title}</h1></div>
        )
  }

  render() {
    const { onSubmit } = this.props;
    const { page } = this.state;
    return (
      <div style={styles.container}>
        <StepsIndicator selectedPage={this.state.page}/>
        <div style={styles.formContainer}>
        {this.renderTitle(this.state.page)}
        {page === 1 && <DealCreationForm onSubmit={this.nextPage} />}
        {page === 2 &&
          <InvoiceCreationForm
            previousPage={this.previousPage}
            onSubmit={onSubmit}
          />}
          </div>
      </div>
    );
  }
}

MainForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};

export default MainForm;
