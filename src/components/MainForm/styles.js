const styles={
    container:{
        display:"flex",
        flexDirection:'column',
        width: "95%",
        color:"black",
        margin:"20px auto",
        height:"500px",
        backgroundColor:"#F5F5F5",
        alignItems:'center',
    },
    formContainer:{
        width:"60%"
    },
    formTitle:{
        textAlign:"center"
    }
}

export default styles;