import React from 'react';
import styles from './styles';

const PrimaryButton = (props) => {
    const { type, disabled, onClick, label} = props;

    return (
        <div style={styles.buttonWrapper} >
            <button style={styles.primaryButton}
             type={type||"submit"}
             disabled={props.disabled||false}
             onClick={props.onClick}
             >{label}</button>
        </div>
    );
}

export default PrimaryButton;
