import {primaryColor, secondaryColor} from '../../theme';

const styles = {
    primaryButton:{
        backgroundColor:primaryColor,
        color: secondaryColor,
        height:"100%",
        width: "100%",
        borderRadius: "20px",
        fontSize:"1.2rem",
    },
    buttonWrapper:{
        height:"40px"
    }
};

export default styles;
