import React from 'react';
import './style.css';

const renderField = ({ input, label, type, meta: { touched, error } }) => (
  <div className="inputWrapper">
    <label >{label.toUpperCase()}</label>
    <div className='input-box'>
      <input  {...input} placeholder={label} type={type} />
      {touched && error && <span className="error-message">{error}</span>}
    </div>
  </div>
);

export default renderField;
