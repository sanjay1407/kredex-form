import React from 'react';
import styles from './styles'

const StepsIndicator = (props) => {
    const step1 = props.selectedPage===1?styles.stepsActive:styles.steps;
    const step2 = props.selectedPage===2?styles.stepsActive:styles.steps;

    return (
        <div style={styles.stepsWrapper}>
            <div style={step1} >Step 1</div>
            <div style={step2} >Step 2</div>
        </div>
    );
}

export default StepsIndicator;
