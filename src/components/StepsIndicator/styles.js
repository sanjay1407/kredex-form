import { primaryColor, secondaryColor } from "../../theme";

const styles = {
  stepsWrapper: {
    marginTop: "30px",
    width: "35%",
    border: "1px solid",
    borderRadius: "20px",
    padding: "1px",
    textAlign: "center"
  },
  steps: {
    display: "inline-block",
    width: "50%",
    verticalAlign: "middle",
    height: "25px"
  },
  stepsActive: {
    verticalAlign: "middle",
    backgroundColor: primaryColor,
    color: secondaryColor,
    display: "inline-block",
    width: "50%",
    height: "25px",
    borderRadius: "20px"
  }
};

export default styles;
