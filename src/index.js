import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import store from "./store";
import showResults from "./showResults";
import MainForm from "../src/components/MainForm";
import Header from '../src/components/Header';
import './styles.css'

const root = document.getElementById("root");

ReactDOM.render(
  <Provider store={store}>
    <div className="root">
      <Header />
      <MainForm onSubmit={showResults} />
    </div>
  </Provider>,
  root
);
