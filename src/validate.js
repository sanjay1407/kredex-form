//Regex for form fields
const dealNameRegex = /^[a-zA-Z ]{1,30}$/;

const isFutureDate = date => {
  var today = new Date();
  today.setHours(0, 0, 0, 0);
  date.setHours(0,0,0,0);
  return date > today ? true : false;
};

const isPastDate = date => {
  var today = new Date();
  date.setHours(0,0,0,0);
  today.setHours(0, 0, 0, 0);
  return date < today ? true : false;
};

const isValidNumber = number => {
  return isNaN(number) ? false : true;
};

const validate = values => {
  const {
    dealName,
    listingDate,
    dealAmount,
    invoiceName,
    issuedDate,
    repaymentDate,
    invoiceAmount
  } = values;
  const errors = {};

  const validateDealName = () => {
    if (!dealNameRegex.test(dealName))
      errors.dealName = "Numbers and Special characters are not allowed";
  };

  const validateListingDate = () => {
    if (isFutureDate(new Date(listingDate)))
      errors.listingDate = "Listing date cannot be future date";
  };

  const validateDealAmount = () => {
    if (!isValidNumber(dealAmount))
      errors.dealAmount = "Please Enter a valid number";
  };

  const validateInvoiceName = () => {
    if (!dealNameRegex.test(invoiceName))
      errors.invoiceName = "Numbers and Special characters are not allowed";
  };

  const validateIssuedDate = () => {
    if (isFutureDate(new Date(issuedDate)))
      errors.issuedDate = "Issued date cannot be future date";
  };

  const validateRepaymentDate = () => {
    if (isPastDate(new Date(repaymentDate)))
      errors.repaymentDate = "Repayment date cannot be past date";
    else if (listingDate < issuedDate || listingDate > repaymentDate)
      errors.repaymentDate =
        "Listing date must be between issued date and repayment date";
  };

  const validateInvoiceAmount = () => {
    if (!isValidNumber(invoiceAmount))
      errors.invoiceAmount = "Please Enter a valid number";
    else if (parseInt(invoiceAmount,10) < parseInt(dealAmount,10))
      errors.invoiceAmount = "Invoice amount must be more than deal amount";
  };

  //===========================================Deal Creation Page==============================================

  //Validate Deal Name
  if (!dealName) {
    errors.dealName = "Required";
  } else validateDealName();

  //Validate Listing Date
  if (!listingDate) {
    errors.listingDate = "Required";
  } else validateListingDate();

  //Validate Deal Amount
  if (!dealAmount) {
    errors.dealAmount = "Required";
  } else validateDealAmount();

  //===========================================Invoice Creation Page============================================

  //Validate Invoice Name
  if (!invoiceName) errors.invoiceName = "Required";
  else validateInvoiceName();

  //Validate Issued Date
  if (!issuedDate) errors.issuedDate = "Required";
  else validateIssuedDate();

  //Validate Repayment Date
  if (!repaymentDate) errors.repaymentDate = "Required";
  else validateRepaymentDate();

  //Validate Invoice Amounnt
  if (!invoiceAmount) errors.invoiceAmount = "Required";
  else validateInvoiceAmount();

  return errors;
};

export default validate;
